function getFileFormatFor(fileType) {
    switch (fileType) {
        case "url":
            return "[InternetShortcut]\nURL=%url%";
        case "desktop":
            return "[Desktop Entry]\nVersion=1.0\nType=Link\nName=%title%\nURL=%url%";
        case "webloc":
            return "<plist version=\"1.0\"><dict>\n<key>URL</key><string>%url%</string>\n</dict></plist>";
        case "html":
            return "<html><head><meta http-equiv=\"refresh\" content=\"0; url=%url%\" \/></head></html>";
    }
}

function cleanTitle(settings, title) {
    return title
        .substring(0, settings.fileLengthLimit)
        .replace(/[*/\\•:#><;,"?|]+/g, " ")
        .replace(/\s\s+/g, ' ')
        .trim();
}

function getFileData(tab, settings, fileType) {
    let title = cleanTitle(settings, tab.title);
    let url = tab.url;
    if (settings.fileType === "webloc") {
        url = url
            .replaceAll("&amp;", "&")
            .replaceAll("&", "&amp;");
    }

    let contents;
    let extension;
    if (fileType === 'custom') {
        contents = settings.customContents;
        extension = settings.customExtension;
    } else {
        extension = fileType;
        contents = getFileFormatFor(fileType);
    }
    contents = contents
        .replaceAll("%url%", url)
        .replaceAll("%title%", tab.title);

    let filename = "";

    if (settings.savePath) {
        filename += settings.savePath + "/";
    }

    filename += `${title}.${extension}`;

    return {
        filename: filename,
        contents: contents
    }
}

function getDownloadSettings(settings, data, incognito) {
    let objectUrl = URL.createObjectURL(new File([data.contents], data.filename));

    let downloadSettings = {
        url: objectUrl,
        filename: data.filename,
        incognito: incognito
    }

    if (settings.saveAs !== null) {
        downloadSettings.saveAs = settings.saveAs;
    }
    return downloadSettings;
}

const FALLBACK_FILE_TYPE = 'html';

function saveLink(tab) {
    browser.storage.local.get().then(
        (settings) => {
            let data = getFileData(tab, settings, settings.fileType);
            let downloadSettings = getDownloadSettings(settings, data, tab.incognito);
            browser.downloads.download(downloadSettings)
                .then(null, (e) => {
                    if (settings.fileType !== FALLBACK_FILE_TYPE) {
                        console.log('Failed to download: falling back to ' + FALLBACK_FILE_TYPE);
                        let fallbackData = getFileData(tab, settings, FALLBACK_FILE_TYPE);
                        browser.downloads.download(getDownloadSettings(settings, fallbackData, tab.incognito))
                            .then(null, console.log);
                    } else {
                        console.log(e);
                    }
                });
        });
}

function getOperatingSystemFileType() {
    if (navigator.platform.startsWith("Linux")) {
        return "desktop";
    } else if (navigator.platform.startsWith("Mac")) {
        return "webloc";
    } else {
        return "url";
    }
}

function getDefaults() {
    return {
        fileType: getOperatingSystemFileType(),
        contextMenu: false,
        savePath: "",
        customContents: 'URL is %url% and title is %title%',
        customExtension: 'txt',
        fileLengthLimit: 200,
        saveAs: null,
        pageAction: false
    };
}

function setDefaults() {
    browser.storage.local.clear();
    browser.storage.local.set(getDefaults()).then(null);
}

function setDefaultsIfEmptyOrInvalid() {
    return new Promise(
        (resolve) => {
            browser.storage.local.get().then(
                (settings) => {
                    let length = Object.keys(settings).length;
                    let defaults = getDefaults();

                    if (length === 0) {
                        setDefaults();
                    }
                    else if (length != Object.keys(defaults).length) {
                        let anyMissing = false;
                        let currentKeys = Object.keys(settings);
                        for (let key of Object.keys(defaults)) {
                            if (currentKeys.indexOf(key) === -1) {
                                anyMissing = true;
                                settings[key] = defaults[key];
                            }
                        }
                        if (anyMissing) {
                            browser.storage.local.set(settings).then(null);
                        }
                    }
                    resolve();
                })
        });
}

function createContextMenu() {
    browser.contextMenus.create({
        id: "save-link",
        title: "Save Link",
        contexts: ["all"]
    });
}

function removeContextMenu() {
    browser.contextMenus.remove("save-link");
}

function setup() {
    browser.storage.local.get().then(
        (settings) => {
            setupContextMenu(settings.contextMenu);
        })
}

function setupContextMenu(on) {
    if (on) {
        createContextMenu();
    }
    else {
        removeContextMenu();
    }
}

function settingsChanged(name, value) {
    switch (name) {
        case "contextMenu":
            setupContextMenu(value);
            break;
    }
}

browser.storage.onChanged.addListener(
    (changes) => {
        for (let name of Object.keys(changes)) {
            if (changes[name].oldValue != changes[name].newValue) {
                settingsChanged(name, changes[name].newValue);
            }
        }
    })

browser.contextMenus.onClicked.addListener((info, tab) => {
    if (info.menuItemId === "save-link") {
        saveLink(tab);
    }
});

browser.browserAction.onClicked.addListener(saveLink);

function main() {
    setDefaultsIfEmptyOrInvalid().then(setup)
}

main();
