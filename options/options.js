function refreshUI() {
    browser.storage.local.get().then(
        (settings) => {
            document.getElementById(settings.fileType + "Option").selected = true;
            document.getElementById("contextMenuCheckBox").checked = settings.contextMenu;
            document.getElementById(settings.saveAs + "Option").selected = true;
            document.getElementById("savePathDir").value = settings.savePath;
            document.getElementById("mainForm").hidden = false;
            document.getElementById("fileLengthLimit").value = settings.fileLengthLimit;
            document.getElementById("customContents").value = settings.customContents;
            document.getElementById("customExtension").value = settings.customExtension;
        });
}

function getSaveAsValue() {
    let select = document.getElementById("saveAsSelect");

    switch (select.options[select.selectedIndex].value) {
        case "null":
            return null;
        case "true":
            return true;
        case "false":
            return false;
    }
}

document.querySelector("#saveButton").addEventListener("click",
    (e) => {
        let select = document.getElementById("linkTypesSelect");
        let fileType = select.options[select.selectedIndex].value;

        let contextMenu = document.getElementById("contextMenuCheckBox").checked
        let saveAs = getSaveAsValue()
        let savePath = document.getElementById("savePathDir").value
        let fileLengthLimit = document.getElementById("fileLengthLimit").value

        let customContents = document.getElementById("customContents").value
        let customExtension = document.getElementById("customExtension").value

        browser.storage.local.set({
            fileType,
            contextMenu,
            saveAs,
            savePath,
            fileLengthLimit,
            customContents,
            customExtension,
        });

    })

function main() {
    refreshUI();
}

main();