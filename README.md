# About
Firefox extension that saves a shortcut file to the webpage

<a href="https://addons.mozilla.org/en-US/firefox/addon/quickcut/"><img src="https://img.shields.io/amo/users/quickcut?label=Firefox%20users" alt="Badge"></img></a>

# Features
Starting from version 1.9 it is now possible to choose a folder to store the links in.

Because of a WebExtension limitation this folder has to be relative to the download folder (meaning they have to be inside it)

This can be useful for mainly 2 reasons:
- To have the downloaded links in a folder neatly seperated from the other downloads.
- Using a symbolic link, redirect this folder to a completely other folder (desktop, for example)



# Tutorial (symbolic links)

I'll be using the desktop folder as an example here, but in theory this should work for any folder. 

It also makes the following assumptions:
- You're using the default download directory (**\<home directory\>/Downloads**)


## Linux
1. As root, run:
```sh
ln -s $HOME/Desktop $HOME/Downloads/Desktop
```
## Windows 10
1. Press <kbd>Win</kbd>+<kbd>X</kbd>
2. Click '**Command Prompt (<u>A</u>dmin)**'
3. Run the following command:
```bat
mklink /D %HOMEPATH%\Downloads\Desktop %HOMEPATH%\Desktop
```

## Mac OS
```sh
# to be written...
```

<br><br>
After this is done, go to the QuickCut preferences and change the Folder to 'Desktop'

Note: you can also change this to Desktop/links in the links folder on the desktop. 


# Donate
This is just a small extension I've written for myself as one of my first programming projects.

I try to update it periodically where needed; feel no obligation to donate but should you really want to:

https://paypal.me/Claderoki
